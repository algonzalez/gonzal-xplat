# gonzal-xplat

Provides utility items to assist in cross-platform (xplat) programming.

## Installation

``` bash
npm install gonzal-stuff --save
```

## Usage

```bash
const path = require('path');
const xplat = require('gonzal.xplat');
const configPath = path.join(xplat.baseDirs.config, '.myAppConfig');

if (xplat.isWindows) {
    // Windows specific setup
} else if (xplat.isMac) {
    // Mac OS X specific setup
} else if (xplat.isLinux) {
    // Linus specific setup
    // NOTE: can use isUnix to cover AIX, BSD, Linux & Solaris
} else {
    // message for unsupported platform
}
```

## Documentation

To create the JSDoc documentation in the docs folder:

```bash
npm run gen:docs
```

To view the documentation:

```bash
npm run view:docs
```

or open `out/index.js` in a web browser.

## Tests

```bash
npm test
```

## Changes

See the CHANGELOG.md file.
