'use strict';

const AppDirs = require('./src/AppDirs');
const baseDirs = require('./src/baseDirs');
const osinfo = require('./src/osinfo');

module.exports = Object.freeze({
  AppDirs,
  baseDirs,
  ...osinfo
});
