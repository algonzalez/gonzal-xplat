'use strict';

const os = require('os');
const path = require('path');
const baseDirs = require('./baseDirs');

/**
 * @class
 * Returns an object that mirrors the properties of '{@link baseDirs}',
 * with the addition of an appended app specific folder
 * to each directory. The app specific folder is composed
 * using the the provided app details.
 * It will be formatted like '/{publisher}/{app}/{version}'
 * omitting any non-specified values.
 *
 * @example
 * const {AppDirs, baseDirs} = require("gonzal-xplat");
 * const appdirs = new AppDirs({app: 'crazy-stuff', publisher: 'gonzal', version: '3.0.0'});
 * // returns '//home/{username}/.config'
 * baseDirs.config;
 * // returns '//home/{username}/.config/gonzal/crazy-stuff/3.0.0'
 * appdirs.config;
 *
 * @param {string|Object} appInfoOrName - name of the app or object with the app details that
 *                                        will be used to build the app specific folder name
 * @param {string} appInfoOrName.app - name of the app
 * @param {string} [appInfoOrName.publisher] - name of entity publishing the app;
 *                                             if supplied, it will be the folder preceding the app folder.
 * @param {string} [appInfoOrName.version] - application version; if supplied,
 *                                           it will be the folder right under the app folder
 */
function AppDirs(appInfoOrName = {}) {
  let appInfo = undefined;

  if (appInfoOrName) {
    if ('string' === typeof appInfoOrName) {
      (appInfo = {}).app = appInfoOrName;
    } else if ('object' === typeof appInfoOrName && appInfoOrName.app) {
      appInfo = appInfoOrName;
    }
  }
  if (!appInfo) {
    throw new Error('must specify an application info object with at least an app property');
  }

  let folders = [];
  if (appInfo.publisher) { folders.push(appInfo.publisher); }
  folders.push(appInfo.app);
  if (appInfo.version) { folders.push(appInfo.version); }

  let appSpecificFolder = path.join.apply(null, folders);

  let appdirs = {}
  for(let key in baseDirs) {
    appdirs[key] = path.join(baseDirs[key], appSpecificFolder);
  }

  return appdirs;
}

module.exports = Object.freeze(AppDirs);
