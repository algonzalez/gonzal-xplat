'use strict';

const os = require('os');
const path = require('path');
const osinfo = require('./osinfo');

/**
 * @member {object} baseDirs - Contains cross-platform values for common base directories.
 *
 * @property {string} app - directory to currently running app
 * @property {string} cache - directory for caching app data
 * @property {string} config - directory for app configuration
 * @property {string} data - directory for app data
 * @property {string} desktop - user's Desktop directory
 * @property {string} documents - user's Documents directory
 * @property {string} downloads - user's Downloads directory
 * @property {string} home - user's Home directory
 * @property {string} movies - user's Movies/Videos directory (same as baseDirs.videos)
 * @property {string} music - user's Music directory
 * @property {string} pictures - user's Pictures directory
 * @property {string} public - user's Public directory
 * @property {string} temp - user's Temp/Tmp directory
 * @property {string} templates - user's Template directory
 * @property {string} videos - user's Movies/Videos directory (same as baseDirs.movies)
 */

function getPlatformSpecificDirs() {
  const homedir = os.homedir();
  const dirs = {};

  dirs.app = path.dirname(process.argv[1]);

  if (osinfo.isWindows) {
    dirs.cache = process.LOCALAPPDATA || path.join(homedir, 'AppData', 'Local');
    dirs.config = process.APPDATA || path.join(homedir, 'AppData', 'Roaming');
    dirs.data = process.APPDATA || path.join(homedir, 'AppData', 'Roaming');
  } else if (osinfo.isMac) {
    dirs.cache = path.join(homedir, 'Library', 'Caches');
    dirs.config = path.join(homedir, 'Library', 'Application Support');
    dirs.data = path.join(homedir, 'Library', 'Application Support');
  } else if (osinfo.isUnix) {
    dirs.cache = process.env.XDG_CACHE_HOME || path.join(homedir, '.cache');
    dirs.config = process.env.XDG_CONFIG_HOME || path.join(homedir, '.config');
    dirs.data = process.env.XDG_DATA_HOME || path.join(homedir, '.local', 'share');
  }

  dirs.desktop = path.join(homedir, 'Desktop');
  dirs.documents = path.join(homedir, 'Documents');
  dirs.downloads = path.join(homedir, 'Downloads');
  dirs.home = homedir;
  dirs.movies = path.join(homedir, osinfo.isMac ? 'Movies' : 'Videos');
  dirs.music = path.join(homedir, 'Music');
  dirs.pictures = path.join(homedir, 'Pictures');
  dirs.public = path.join(homedir, 'Public');
  dirs.temp = os.tmpdir();
  dirs.templates = path.join(homedir, "Templates");
  dirs.videos = dirs.movies;

  if (osinfo.isUnix) {
    dirs.desktop = process.env.XDG_DESKTOP_DIR || dirs.desktop;
    dirs.documents = process.env.XDG_DOCUMENTS_DIR || dirs.documents;
    dirs.downloads = process.env.XDG_DOWNLOAD_DIR || dirs.downloads;
    dirs.movies = process.env.XDG_VIDEOS_DIR || dirs.movies;
    dirs.music = process.env.XDG_MUSIC_DIR || dirs.music;
    dirs.pictures = process.env.XDG_PUBLICSHARE_DIR || dirs.pictures;
    dirs.public = process.env.XDG_PUBLICSHARE_DIR || dirs.public;
    dirs.templates = process.env.XDG_TEMPLATES_DIR || dirs.templates;
    dirs.videos = process.env.XDG_VIDEOS_DIR || dirs.videos;
  }

  return dirs;
}

module.exports = Object.freeze(getPlatformSpecificDirs());
