'use strict';

const os = require('os');

/** true if running on an AIX platform; otherwise false */
const isAIX = process.platform === 'aix';
/** true if running on a FreeBSD or OpenBSD platforms; otherwise false */
const isBSD = process.platform === 'freebsd' || process.platform === 'openbsd';
/** true if running on a Linux platform; otherwise false */
const isLinux = process.platform === 'linux';
/** true if running on a Mac OS X platform; otherwise false */
const isMac = process.platform === 'darwin';
/** true if running on a Solaris platform; otherwise false */
const isSolaris = process.platform === 'sunos';
/** true if running on a Unix based platform (AIX, BSD, Linux, Solaris); otherwise false */
const isUnix = isLinux || isAIX || isBSD || isSolaris;
/** true if running on a Windows platform; otherwise false */
const isWindows = process.platform === 'win32';

/** carriage-return character(s) */
const CR = '\r';
/** carriage-return and line-feed character(s) */
const CRLF = '\r\n';
/** @description end-of-line character(s) */
const EOL = os.EOL;
/** @description line-feed character(s) */
const LF = '\n';

module.exports = Object.freeze({
  isAIX,
  isBSD,
  isLinux,
  isMac,
  isSolaris,
  isUnix,
  isWindows,

  CR,
  CRLF,
  EOL,
  LF,
});
