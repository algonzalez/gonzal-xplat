# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 1.0.0 - 2018-08-26
### Added
- project files CHANGELOG.md, LICENSE.txt, README.md, package.json & package-lock.json 
- development files .editorconfig & .gitignore
- code to check if running on a specific platform
- code to expose cross-platform values for common base directories
- AppDirs class to add app specific subfolders to baseDirs
- tests for AppDirs class
