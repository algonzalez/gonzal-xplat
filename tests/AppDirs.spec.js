const {AppDirs, baseDirs} = require('..');
const path = require('path');

const dirsWithAppendedFolder = ['app', 'cache', 'config', 'data', 'temp'];
const app = 'awesome-app';
const publisher = 'yours-truly';
const version = '42.1.5';

function checkDirs(appdirs, folder) {
  for (let key in appdirs) {
    let appdir = appdirs[key];
    expect(appdir.endsWith(folder)).toBe(true);
    expect(appdir.startsWith(baseDirs[key]));
  }
}

describe('AppDirs', () => {
  it('with app name as string, appends the app name to the basedirs', () => {
    let appdirs = new AppDirs(app);
    let folder = path.sep + app;
    checkDirs(appdirs, folder);
  });
  it('with app name in object, appends the app name to the basedirs', () => {
    let appdirs = new AppDirs({app: app});
    let folder = path.sep + app;
    checkDirs(appdirs, folder);
  });
  it('with publisher and app name, appends the publisher and app name to the basedirs', () => {
    let appdirs = new AppDirs({publisher: publisher, app: app});
    let folder =  path.sep + path.join(publisher, app);
    checkDirs(appdirs, folder);
  });
  it('with app name and version, appends the app name and version to the basedirs', () => {
    let appdirs = new AppDirs({app: app, version: version});
    let folder =  path.sep + path.join(app, version);
    checkDirs(appdirs, folder);
  });
  it('with publisher, app name and version, appends the publisher, app name and version to the basedirs', () => {
    let appdirs = new AppDirs({publisher: publisher, app: app, version: version});
    let folder =  path.sep + path.join(publisher, app, version);
    checkDirs(appdirs, folder);
  });
  it('throws when no args', () => {
    expect(()=>new AppDirs()).toThrow();
  });
  it('throws when arg object does not include required app', () => {
    expect(()=>new AppDirs({publisher: publisher})).toThrow();
  });
});
